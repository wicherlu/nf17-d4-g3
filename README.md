# PROJET NF17 PRINTEMPS 2020
 
 *Membre du groupe 3 :*
*  Anthony TORDJMANN
*  Simon HAMON
*  Lucas WICHER
*  Mathis BOSTON
  

**Architecture du projet :** La réalisation d'une base de données est une tâche complexe. On divise le travail en 4 étapes pour faciliter sa réalisation :
    
1. [NDC.md](../NDC.md) : Note de Clarification est une reformulation de ce qui est demandé où l'on a pu préciser, ajouter ou supprimer des éléments tout en les justifiant. Nous nous sommes basé sur cette 
       référence pour la suite du projet.
2. [MCD.plantuml](../MCD.plantuml) : Le Modèle Conceptuel de Données permet de représenter le problème à l'aide de représentations graphiques. On utilisera la modélisation UML et le format plantuml.
     
3. [MLD.txt](../MLD.txt) : Le modèle logique de données relationnel nous de représenter une base de données relationnelles. On y voit les tables avec leurs attributs, propriétés, domaines, contraintes...
    
4. [Code_SQL.txt](../Code_SQL.txt): Ensemble du code SQL nécessaire pour créer la base de données avec les contraintes, tables, attributs lors des étapes précédentes. 
        On trouve aussi le code correspondant l'insertion des données tests ainsi que le code nécessaire à la création de vues répondants partiellement aux besoins. 

5. [Documentation ](../Documentation.pdf) : Il s'agit de la documentation générale du projet, elle reprend et justifie globalement nos choix et nos actions lors des différents étapes du projet afin que le client puisse 
    comprendre la manière dont la base de donnée a été créé.