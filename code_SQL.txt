DROP DATABASE IF EXISTS Bibliotheque ;
CREATE DATABASE Bibliotheque;
DROP TABLE IF EXISTS Contributeur, Realisateur, Acteur, Compositeur, Interprete, Auteur, Ressource, Modification, Genre, Livre, Musique, Film, Exemplaire, Reservation, Personnel, Adherent, Emprunt, Profil, Profil_adherent, Sanction, Interet, Blacklist  CASCADE;

CREATE TABLE Contributeur(
nom VARCHAR,
prenom VARCHAR,
date_naissance DATE,
nationalite VARCHAR NOT NULL,
PRIMARY KEY (nom, prenom, date_naissance)
);

CREATE TABLE Personnel(
login VARCHAR PRIMARY KEY,
pwd VARCHAR NOT NULL,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL,
mail VARCHAR NOT NULL,
adresse VARCHAR NOT NULL
);

CREATE TABLE Adherent(
login VARCHAR PRIMARY KEY,
pwd VARCHAR NOT NULL,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL,
mail VARCHAR NOT NULL,
num_carte INTEGER UNIQUE,
adhesion VARCHAR CHECK( adhesion IN ('passee','actuelle')),
nb_emprunt INTEGER CHECK(nb_emprunt>=0),
nb_sanction INTEGER CHECK(nb_sanction>=0),
elegibilite VARCHAR CHECK(elegibilite IN ('OK', 'suspendu', 'blackliste'))
);

CREATE TABLE Genre(
nom VARCHAR PRIMARY KEY
);

CREATE TABLE Ressource(
code INTEGER PRIMARY KEY,
titre VARCHAR NOT NULL,
date_apparition DATE NOT NULL,
editeur VARCHAR NOT NULL,
genre VARCHAR NOT NULL,
ajout VARCHAR,
FOREIGN KEY (ajout) REFERENCES Personnel(login),
FOREIGN KEY (genre) REFERENCES Genre(nom)
);

CREATE TABLE Livre(
ressource INTEGER PRIMARY KEY,
isbn VARCHAR NOT NULL,
resume VARCHAR,
langue VARCHAR NOT NULL,
FOREIGN KEY(ressource) REFERENCES Ressource(code)
);

CREATE TABLE Musique(
ressource INTEGER PRIMARY KEY,
longueur TIME,
FOREIGN KEY(ressource) REFERENCES Ressource(code)
);

CREATE TABLE Film(
ressource INTEGER PRIMARY KEY,
longueur TIME,
langue VARCHAR NOT NULL CHECK( langue IN ('anglais','francais', 'espagnol', 'italien', 'russe', 'suedois')),
FOREIGN KEY(ressource) REFERENCES Ressource(code)
);

CREATE TABLE Exemplaire(
num_exemplaire INTEGER,
ressource INTEGER,
disponibilite VARCHAR CHECK( disponibilite IN('emprunte', 'reserve', 'libre')),
etat VARCHAR CHECK( etat IN('neuf', 'bon', 'abime', 'perdu')),
ajout VARCHAR,
FOREIGN KEY (ajout) REFERENCES Personnel(login),
FOREIGN KEY (ressource) REFERENCES Ressource(code),
PRIMARY KEY (num_exemplaire, ressource)
);

CREATE TABLE Realisateur(
nom VARCHAR ,
prenom VARCHAR,
date_naissance DATE,
codefilm INTEGER,
FOREIGN KEY (nom,prenom,date_naissance) REFERENCES Contributeur(nom,prenom,date_naissance),
FOREIGN KEY (codefilm) REFERENCES Film(ressource)
);

CREATE TABLE Acteur(
nom VARCHAR,
prenom VARCHAR ,
date_naissance DATE,
codefilm INTEGER,
FOREIGN KEY (nom,prenom,date_naissance) REFERENCES Contributeur(nom,prenom,date_naissance),
FOREIGN KEY (codefilm) REFERENCES Film(ressource)
);

CREATE TABLE Auteur(
nom VARCHAR,
prenom VARCHAR,
date_naissance DATE,
codelivre INTEGER,
FOREIGN KEY (nom,prenom,date_naissance) REFERENCES Contributeur(nom,prenom,date_naissance),
FOREIGN KEY (codelivre) REFERENCES Livre(ressource)
);

CREATE TABLE Compositeur(
nom VARCHAR,
prenom VARCHAR,
date_naissance DATE,
codemusique INTEGER,
FOREIGN KEY (nom,prenom,date_naissance) REFERENCES Contributeur(nom,prenom,date_naissance),
FOREIGN KEY (codemusique) REFERENCES Musique(ressource)
);

CREATE TABLE Interprete(
nom VARCHAR,
prenom VARCHAR,
date_naissance DATE,
codemusique INTEGER,
FOREIGN KEY (nom,prenom,date_naissance) REFERENCES Contributeur(nom,prenom,date_naissance),
FOREIGN KEY (codemusique) REFERENCES musique(ressource)
);


CREATE TABLE Modification(
ressource INTEGER,
personnel VARCHAR,
date_modification DATE NOT NULL,
description VARCHAR NOT NULL,
FOREIGN KEY (ressource) REFERENCES Ressource(code),
FOREIGN KEY (personnel) REFERENCES Personnel(login)
);

CREATE TABLE Emprunt(
id_emprunt INTEGER PRIMARY KEY,
login VARCHAR,
date_emprunt DATE NOT NULL,
duree INTEGER,
num_exemplaire INTEGER,
ressource INTEGER,
date_retour DATE,
FOREIGN KEY (login) REFERENCES Adherent(login),
FOREIGN KEY (num_exemplaire,ressource) REFERENCES Exemplaire(num_exemplaire,ressource)
);

CREATE TABLE Profil(
nom VARCHAR PRIMARY KEY
);

CREATE TABLE Profil_adherent(
profil VARCHAR,
adherent VARCHAR,
FOREIGN KEY (profil) REFERENCES Profil(nom),
FOREIGN KEY (adherent) REFERENCES Adherent(login)
);

CREATE TABLE Reservation (
id_reservation INTEGER,
reservataire_login VARCHAR,
date_max_retrait DATE NOT NULL,
PRIMARY KEY (id_reservation, reservataire_login),
FOREIGN KEY (id_reservation) REFERENCES Emprunt(id_emprunt),
FOREIGN KEY (reservataire_login) REFERENCES Adherent(login)
);



CREATE TABLE Sanction (
id_sanction INTEGER PRIMARY KEY  ,
id_emprunt INTEGER ,
type VARCHAR CHECK(type IN('suspension','remboursement',NULL)),  
etat VARCHAR CHECK(etat IN('paye','non paye',NULL)),
debut DATE,
duree INTEGER,
FOREIGN KEY (id_emprunt) REFERENCES Emprunt(id_emprunt)
);

CREATE TABLE Blacklist(
date_sanction DATE NOT NULL,
login_adherent VARCHAR,
login_personnel VARCHAR,
FOREIGN KEY (login_adherent) REFERENCES Adherent(login),
FOREIGN KEY (login_personnel) REFERENCES Personnel(login)
);

CREATE TABLE Interet(
profil VARCHAR,
genre VARCHAR,
FOREIGN KEY (profil) REFERENCES Profil(nom),
FOREIGN KEY (genre) REFERENCES Genre(nom)
);


INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Mariah', 'Carey', '1970-03-27', 'americaine');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Michael', 'Bay', '1965-02-17', 'americaine');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Henri', 'Bergson', '2020-04-21', 'francaise');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('William', 'Shakespeare', '1564-04-26', 'britannique');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Victor', 'Hugo', '1802-02-26', 'francaise');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Joanne', 'Rowling', '1965-07-31', 'britannique');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('John', 'Dewey', '1859-08-20', 'americaine');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('George', 'Martin', '1948-09-20', 'americaine');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Stanley', 'Kubrick', '1928-07-26', 'americaine');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Withney', 'Houston', '1963-08-09', 'americaine');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Serge', 'Gainsbourg', '1928-04-02', 'francaise');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Dwayne', 'Johnson', '1972-05-02', 'americain');
INSERT INTO Contributeur (prenom, nom, date_naissance, nationalite) VALUES ('Jack', 'Nicholson', '1939-04-22', 'americain');



INSERT INTO Personnel(login,pwd,nom,prenom,mail,adresse) VALUES ('atordjma', 'pommme222','Tordjmann','Anthony','atordjma@etu.utc.fr','4 rue des vignes beauvoisin Villeron');
INSERT INTO Personnel(login,pwd,nom,prenom,mail,adresse) VALUES ('shamon', 'Lzbr06','Hamon','Simon','shamon@etu.utc.fr','27 boulevard des etats unis Compiègne');
INSERT INTO Personnel(login,pwd,nom,prenom,mail,adresse) VALUES ('bmathis', 'Htrlp46','Boston','Mathis','bmathis@yahoo.fr','14 rue des hirondelles Jaux');
INSERT INTO Personnel(login,pwd,nom,prenom,mail,adresse) VALUES ('lwicher', 'Pdhrng72','Wicher','Lucas','lucas.wicher@gmail.fr','21 avenue des jonquilles');


INSERT INTO Adherent(login ,pwd ,nom, prenom ,mail ,num_carte ,adhesion, nb_emprunt,nb_sanction,elegibilite) VALUES ('abergou', 'Mplhj78', 'Bergougnoux', 'Aline', 'aline.bergougnoux@hotmail.com', 24601, 'actuelle',1, 1 , 'OK');
INSERT INTO Adherent(login ,pwd ,nom, prenom ,mail ,num_carte ,adhesion, nb_emprunt,nb_sanction,elegibilite) VALUES ('jkhezzan', 'Lhngdt42', 'Khezzane', 'Josh', 'jkhezz@hotmail.com', 29856, 'actuelle',0, 3 , 'OK');
INSERT INTO Adherent(login ,pwd ,nom, prenom ,mail ,num_carte ,adhesion, nb_emprunt,nb_sanction,elegibilite) VALUES ('emari', 'Pudgra80', 'Marie', 'Etienne', 'etienne60@wanadoo.com', 14579, 'passee',1, 5 , 'blackliste');
INSERT INTO Adherent(login ,pwd ,nom, prenom ,mail ,num_carte ,adhesion, nb_emprunt,nb_sanction,elegibilite) VALUES ('ablgr', 'Lhngdt42', 'Boulanger', 'Arthur', 'ablgr@hotmail.com', 29857, 'actuelle',0, 3 , 'suspendu');
INSERT INTO Adherent(login ,pwd ,nom, prenom ,mail ,num_carte ,adhesion, nb_emprunt,nb_sanction,elegibilite) VALUES ('stau', 'Pudgra80', 'Tau', 'Sami', 'sami60@wanadoo.com', 14580, 'passee',1, 5 , 'suspendu');

INSERT INTO Genre(nom) VALUES ('BD');
INSERT INTO Genre(nom) VALUES ('Drama');
INSERT INTO Genre(nom) VALUES ('Variete');
INSERT INTO Genre(nom) VALUES ('Opera');
INSERT INTO Genre(nom) VALUES ('Action');
INSERT INTO Genre(nom) VALUES ('Dessin anime');
INSERT INTO Genre(nom) VALUES ('Hard Rock');
INSERT INTO Genre(nom) VALUES ('Pop');
INSERT INTO Genre(nom) VALUES ('Philosophie');


INSERT INTO Ressource(code,titre,date_apparition,editeur,genre,ajout) VALUES (17,'Le rire', '1900-05-01', 'La revue de Paris', 'Philosophie', 'atordjma');
INSERT INTO Ressource(code,titre,date_apparition,editeur,genre,ajout) VALUES (19,'Le Poinconneur des lilas', '1968-06-15', 'Radio France', 'Variete', 'shamon');
INSERT INTO Ressource(code,titre,date_apparition,editeur,genre,ajout) VALUES (86,'Othello', '1604-10-01', 'Theatre Royal', 'Drama', 'bmathis');
INSERT INTO Ressource(code,titre,date_apparition,editeur,genre,ajout) VALUES (94,'Transformers', '2009-10-14', '20th century fox', 'Action', 'lwicher');
INSERT INTO Ressource(code,titre,date_apparition,editeur,genre,ajout) VALUES (104,'2001 a space odissey', '1968-09-27', 'MGM', 'Philosophie', 'shamon');
INSERT INTO Ressource(code,titre,date_apparition,editeur,genre,ajout) VALUES (213,'All I want for christmas is you', '1994-07-01', 'Walter Afanasieff, Mariah Carey', 'Pop', 'atordjma');
INSERT INTO Modification (ressource,personnel,date_modification,description) VALUES( 86, 'bmathis', '2020-02-03', 'changement genre');
INSERT INTO Modification (ressource,personnel,date_modification,description) VALUES  (213, 'shamon', '2020-01-12', 'erreur dans le titre');

INSERT INTO Livre ( ressource, isbn, resume, langue) VALUES ( 17, 9782765409120, 'La Revue de Paris est une revue littéraire française', 'francais') ;
INSERT INTO Livre ( ressource, isbn, resume, langue) VALUES (86, 9781560772545, 'Othello, le Maure de Venise (Othello, the Moor of Venice en anglais) est une tragédie de William Shakespeare, jouée pour la première fois en 1604', 'francais');

INSERT INTO Musique (ressource,longueur) VALUES (19,'00:02:00');
INSERT INTO Musique (ressource,longueur) VALUES (213,'00:04:01');

INSERT INTO Film (ressource, longueur, langue) VALUES (104,'02:44:00', 'anglais');
INSERT INTO Film (ressource, longueur, langue) VALUES (94, '02:24:01', 'anglais');


INSERT INTO Realisateur (prenom, nom,date_naissance,codefilm) VALUES ('Stanley', 'Kubrick', '1928-07-26',104);
INSERT INTO Realisateur (prenom,nom, date_naissance,codefilm) VALUES ('Michael', 'Bay', '1965-02-17',94);

INSERT INTO Compositeur( prenom, nom, date_naissance, codemusique ) VALUES ('Serge', 'Gainsbourg', '1928-04-02',19);
INSERT INTO Compositeur (prenom, nom, date_naissance, codemusique) VALUES ('Mariah', 'Carey', '1970-03-27',213);

INSERT INTO Interprete (prenom, nom, date_naissance, codemusique)  VALUES  ('Serge', 'Gainsbourg', '1928-04-02',19);
INSERT INTO Interprete (prenom, nom, date_naissance, codemusique) VALUES ('Mariah', 'Carey', '1970-03-27',213);

INSERT INTO Acteur (prenom, nom,date_naissance,codefilm) VALUES ('Dwayne', 'Johnson', '1972-05-02', 94);
INSERT INTO Acteur (prenom, nom,date_naissance,codefilm) VALUES ('Jack', 'Nicholson', '1939-04-22',104);

INSERT INTO Auteur (prenom, nom, date_naissance, codelivre) VALUES ('William', 'Shakespeare', '1564-04-26', 86);

INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (1, 17, 'libre', 'neuf', 'atordjma');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (2, 17, 'libre', 'bon', 'shamon');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (3, 17, 'libre', 'perdu', 'bmathis');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (1, 19, 'libre', 'neuf', 'lwicher');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (1, 86, 'libre', 'abime', 'lwicher');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (2, 86, 'emprunte', 'abime', 'shamon');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (1, 94, 'libre', 'neuf', 'bmathis');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (2, 94, 'emprunte', 'perdu', 'atordjma');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (1, 104, 'libre', 'neuf', 'shamon');
INSERT INTO Exemplaire(num_exemplaire,ressource,disponibilite,etat,ajout) VALUES (1, 213, 'reserve', 'perdu', 'atordjma');

INSERT INTO Profil (nom) VALUES ('jeunesse');
INSERT INTO Profil (nom) VALUES ('senior');
INSERT INTO Profil (nom) VALUES ('famille');

INSERT INTO Profil_adherent (profil, adherent) VALUES ('jeunesse','emari');

INSERT INTO Blacklist (date_sanction, login_adherent, login_personnel) VALUES ('2020-12-25', 'emari', 'atordjma');

INSERT INTO Interet (profil, genre) VALUES ('jeunesse', 'BD');


INSERT INTO Emprunt (id_emprunt, login, date_emprunt, duree, num_exemplaire, ressource) VALUES (1, 'abergou', '2020-01-10', 6, 1, 86);
INSERT INTO Emprunt(id_emprunt, login, date_emprunt, duree, num_exemplaire, ressource) VALUES (2, 'emari', '2020-02-03',4, 2, 17);
INSERT INTO Emprunt(id_emprunt, login, date_emprunt, duree, num_exemplaire, ressource, date_retour) VALUES (3, 'jkhezzan', '2020-02-03', 5, 1, 17, '2020-03-20');
INSERT INTO Emprunt (id_emprunt, login, date_emprunt, num_exemplaire, ressource) VALUES (4, 'stau', '2020-09-10', 1, 86);
INSERT INTO Emprunt(id_emprunt, login, date_emprunt, num_exemplaire, ressource) VALUES (5, 'ablgr', '2020-05-03', 2, 17);
INSERT INTO Emprunt (id_emprunt, login, date_emprunt, num_exemplaire, ressource) VALUES (6, 'stau', '2020-06-10', 1, 86);



INSERT INTO Sanction (id_sanction, id_emprunt, type, etat, debut, duree) VALUES (1, 2, 'remboursement', 'non paye', NULL, NULL);

INSERT INTO Sanction (id_sanction, id_emprunt, type, etat, debut, duree) VALUES (2, 1, 'suspension', NULL, '2020-04-14', 14);

INSERT INTO Sanction (id_sanction, id_emprunt, type, etat, debut, duree) VALUES (3, 5, 'remboursement', 'non paye', NULL, NULL);

INSERT INTO Sanction (id_sanction, id_emprunt, type, etat, debut, duree) VALUES (4, 4, 'suspension', NULL, '2020-05-14', 14);

INSERT INTO  Reservation (id_reservation, reservataire_login, date_max_retrait) VALUES (1, 'emari', '2020-04-18');

INSERT INTO  Reservation (id_reservation, reservataire_login, date_max_retrait) VALUES (2, 'jkhezzan', '2020-06-14');


-- Vue pour les exemplaires empruntés

DROP VIEW IF EXISTS ExemplairesEmpruntes;
CREATE VIEW ExemplairesEmpruntes (Ressource,NumExemplaire, Emprunteur, date_emprunt, date_retour)
AS
SELECT Ressource.titre, Exemplaire.num_exemplaire,  login, date_emprunt, date_retour
FROM Exemplaire
JOIN  Ressource
ON Exemplaire.ressource = Ressource.code
JOIN Emprunt
ON Emprunt.num_exemplaire = Exemplaire.num_exemplaire AND Emprunt.ressource = Exemplaire.ressource
ORDER BY login;

-- Vue pour les exemplaires libres
DROP VIEW IF EXISTS ExemplairesLibres;
CREATE VIEW ExemplairesLibres (Ressource, NbExemplaire)
AS 
SELECT Ressource.titre, COUNT(Exemplaire.ressource) 
FROM Exemplaire
JOIN Ressource
ON Exemplaire.ressource = Ressource.code
WHERE Exemplaire.disponibilite = 'libre'
GROUP BY Ressource.titre;



-- Vue pour Top Semaine
DROP VIEW IF EXISTS TopSemaine ;
CREATE VIEW TopSemaine (Ressource, Nbemprunts)
AS 
SELECT Ressource,COUNT(ExemplairesEmpruntes.ressource) AS NB
FROM ExemplairesEmpruntes
WHERE date_emprunt >= CURRENT_DATE - 7
GROUP BY Ressource, date_emprunt
ORDER BY NB DESC
LIMIT 10;


-- Vue pour les adhérents suspendus actuellement

DROP VIEW IF EXISTS AdherentsSuspendus ;
CREATE VIEW AdherentsSuspendus (nom, prenom, mail,type,debut,duree)
AS
SELECT Adherent.nom, Adherent.prenom, Adherent.mail, Sanction.type, Sanction.debut, Sanction.duree
FROM Emprunt
JOIN  Sanction
ON Emprunt.id_emprunt = Sanction.id_emprunt
JOIN Adherent
ON Adherent.login = Emprunt.login
WHERE Adherent.elegibilite = 'suspendu'
ORDER BY nom;

-- Vue pour les emprunts en cours

DROP VIEW IF EXISTS Emprunts_en_cours;
CREATE VIEW Emprunts_en_cours (Ressource,NumExemplaire, Emprunteur, date_emprunt)
AS
SELECT Ressource.titre, Exemplaire.num_exemplaire,  login, date_emprunt
FROM Exemplaire
JOIN  Ressource
ON Exemplaire.ressource = Ressource.code
JOIN Emprunt
ON Emprunt.num_exemplaire = Exemplaire.num_exemplaire AND Emprunt.ressource = Exemplaire.ressource
WHERE Emprunt.date_retour IS NULL
ORDER BY login;

-- Vue pour les emprunts en retard
DROP VIEW IF EXISTS Emprunts_en_retard;
CREATE VIEW Emprunts_en_retard (Ressource,NumExemplaire, Emprunteur, date_emprunt)
AS
SELECT Ressource.titre, Exemplaire.num_exemplaire,  login, date_emprunt
FROM Exemplaire
JOIN  Ressource
ON Exemplaire.ressource = Ressource.code
JOIN Emprunt
ON Emprunt.num_exemplaire = Exemplaire.num_exemplaire AND Emprunt.ressource = Exemplaire.ressource
WHERE Emprunt.date_retour IS NULL AND date_emprunt <= CURRENT_DATE - 14;


-- Vue Exemplaire en reservation 
DROP VIEW IF EXISTS ExemplairesReserves;
CREATE VIEW ExemplairesReserves (Ressource, num_exemplaire, login, date_max_retrait)
AS 
SELECT Ressource.titre, Emprunt.num_exemplaire, reservataire_login, date_max_retrait
FROM Reservation
JOIN Emprunt
ON id_reservation = id_emprunt
JOIN Exemplaire
ON Emprunt.num_exemplaire = Exemplaire.num_exemplaire AND Emprunt.ressource = Exemplaire.ressource
JOIN Ressource
ON Exemplaire.ressource = Ressource.code;
