**Ressource**

    Type : {livre, film, musique};
    Code : integer #unique;
    Titre : string;
    Date_apparition : date
    Editeur : string ;
    Genre : string ;
    Classification : string ;
    
*Classe mère de : Livre, Musique, Film*

**Exemplaire**

    Document : integer (=>Ressource.Code) ;
    Disponibilité : {Emprunté, Réservé, Libre} ;
    Etat : {neuf, bon, abîmé, perdu} ;
    Num_exemplaire : integer ; 
    Prix : integer ;
    
*(Document, Num_exemplaire) #primary key*

    
**Livre**

*Hérite de Ressource*

    ISBN : string ;
    Résumé : string ;
    Langue : string ;
    
**Musique**

*Hérite de Ressource*

    Longueur : durée ;
    
**Film**

*Hérite de Ressource*

    Langue : string ;
    Longueur : durée ;
    
**Contributeur**

    Nom : string ;
    Prénom : string ;
    Date_naissance : date
    Nationalité : string ;
    
    
*(Nom, Prénom, Contribution) #primary key*
*Classe mère de Auteur, Interprète, Compositeur, Réalisateur et Acteur* 

**Contribution**

    Nom : string (=>Contributeur.Nom) ;
    Prénom : string (=>Contributeur.Prénom) ;
    Ressource : integer (=>Ressource.Code) ;

**Auteur**

*Hérite de Contributeur*

    Livre : integer (=>Livre.Code);

**Interprète**

*Hérite de Contributeur*

    Musique : integer (=>Musique.Code) ;


**Compositeur**

*Hérite de Contributeur*

    Musique : integer (=>Musique.Code) ;

**Réalisateur**

*Hérite de Contributeur*

    Film : integer (=>Film.Code) ;

**Acteur**

*Hérite de Contributeur*

    Film : integer (=>Film.Code) ;

   
**Compte**

    login : string ;
    pwd : string ;
    Mail : string ;
    
*(login, pwd) #primary key*
*Classe mère de Personnel et Adhérent*
   
**Personnel**

*Hérite de Compte*

    Adresse : string ;


**Adhérent**

*Hérite de Compte*

    Num_carte : integer #unique;
    Num_tél : string ;
    Adhésion : {passée, actuelle} ;
    Nb_emprunt : integer ;
    Nb_sanctions : integer ;
    Eligibilité : {OK, suspendu, blacklisté} ;
    

**Emprunt**

    Document : integer (=>Exemplaire.Document) ;
    Numéro : integer (=>Exemplaire.Num_exemplaire)
    login_adhérent : string (=>Adhérent.login) ;
    pwd_adhérent : string (=>Adhérent.pwd) ;
    Date_emprunt : date ;
    Durée : durée ;
    
*(Document, login_adhérent, pwd_adhérent, Date_emprunt) #primary key*
    
**Retour**

    Emprunt : emprunt ;
    Date_retour : date ;
    Retard : (Méthode) ;
    
*(emprunt) #primary key*

    
**Sanction**

    Type : {Retard, remboursement} ;
    Emprunt : emprunt ;
    Début_suspension : date ;
    Durée_suspension : durée ;
    
*(Document, login_adhérent, pwd_adhérent) #primary key*

*Durée_suspension n'existe que si Type = Retard*

**Réservation**

    login_adhérent : string (=>Adhérent.login) ;
    pwd_adhérent : string (=>Adhérent.pwd) ;
    Document : integer (=>Ressource.Code) ;
    
*(Document, login_adhérent, pwd_adhérent) #primary key*

**Profil**

    Tranche d'âge : Intervalle;
    Genre : string ;

**Contraintes supplémentaires**

    (Personnel.login,Personel.pwd) != (Adhérent.login, Adhérent.pwd) pour tous les couples dans Personnel et Adhérent
    (Exemplaire.Disponibilité = Libre) AND (Exemplaire.Etat = ‘bon’ OR ‘neuf’)

